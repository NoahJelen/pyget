#!/usr/bin/env python
import urllib.request
import sys
import requests

#basic http request
def request(address):
    #url to request from
    address = parse_address(address)
    req = urllib.request.Request(address)
    
    #make the request
    with urllib.request.urlopen(req) as response:
        the_page = response.read()
    
    #return the data
    return the_page

#add 'https://' if it doesn't exist
def parse_address(address):
    if "http://" in address:
        return address
    if "https://" in address:
        return address
    return "https://"+address
flag = sys.argv[1]
    
#save http output to a file
if flag == "-o":
    #make sure address is correct
    address=sys.argv[2]
    print(address)
    
    #request data
    output=request(address)
    
    #output file name
    outfile=sys.argv[3]
    
    #open the file
    f= open(outfile,"w+")
    
    #write to file and close it
    f.write(str(output))
    f.close()
    
#download a file
elif flag == "-d":
    url = parse_address(sys.argv[2])
    outfile = sys.argv[3]
    r = requests.get(url)

    with open(outfile, 'wb') as f:
        f.write(r.content)
        
#help
elif flag == "-h":
    print("pyget help:\nsyntax: pyget.py <url>\nArguments: -o: save output to file\n-d: download file\n-h: show this help\npyget.py -o <url> <filename>\npyget.py -d <url of file> <filename>")

#basic http request
else:
    print(request(flag))
